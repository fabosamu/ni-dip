% !TeX spellcheck = sk_SK-Slovak

\documentclass[../DIP-Fabo-Samuel.tex]{subfiles}

\begin{document}
	
\chapter{Spracovanie prirodzenej reči}

Spracovanie prirodzenej reči (anlg. \gls{nlp}) je vedecká disciplína zaoberajúca sa prirodzenou ľudskou rečou, spracovaním textu, a hovoreného slova.
Jedna z prvých zmienok siaha až do roku 1950, kedy Alan Turing sformuloval tzv. Turingov test, ktorý sa pokúša dať odpoveď na otázku či je nejaký stroj inteligentný. \cite{TURING1950}

Táto disciplína je veľmi široká a pokrýva témy, ako napríklad odpovedanie na otázky (question answering), strojový preklad (machine translation), sumarizácia textu (text summarization) alebo generovanie popisu pre nejaký obrázok (image text generation). Popíšme jednotlivé obdobia vývoja \gls{nlp}:

\begin{itemize}
	\item
	\textit{Ručne písané pravidlá} -- populárna technika už pri počiatkoch počítačov, keď nebolo k dispozícii dostatok výpočtovej sily. Pravidlá -- podmienky -- boli tvorené ručne a expertne. Hlavnou výhodou bola možnosť absolútnej kontroly výstupu, takže nájsť problematické miesta aplikácie a následne ich opraviť nebolo ťažké. Oproti tomu bolo veľmi intelektuálne aj časovo vyčerpávajúce vytvoriť zoznam všetkých možných otázok a odpovedí. Preto sa dnes už táto metóda skoro nepoužíva. Priekopníkom chatbotov sa stala tzv. ELIZA, ktorá bola vyvinutá pod taktovkou Josepha Weizenbauma z MIT \cite{Weizenbaum1966} v roku 1966.
	\item 
	\textit{Štatistický prístup} -- používaný okolo prelomu milénií (približne 1990-2010). So vznikom mnohých webových stránok a rozširovaním internetu do rôznych kútov sveta vzniklo veľké množstvo textu, ktorý bol voľne dostupný. Tak vznikali tzv. korpusy, ktoré mohli byť použité napríklad na strojové preklady pomocou techník nesupervizovaného učenia. Samotné učenie sa stalo možným vďaka zvyšovaniu výkonu počítačov a superpočítačov.
	\item 
	\textit{Moderné \gls{nlp}} -- v posledných rokoch získalo mnoho úspechov aj vďaka vysokým výkonom moderných počítačov, používania grafických kariet na náročné výpočty. Od popisu perceptronu \cite{Freund1999} až po jeho \uv{znovuobjavenie} a rôzne úpravy a variácie použité na množstvo rôznych problémov. Viac o neurónových sieťach si povieme v nasledujúcej sekcii.
\end{itemize}

V tejto práci sa budeme zaoberať hlavne prístupom štatistickým, kde kvalita výsledkov je závislá na kvalite a štatistickému -- strojovému -- porozumeniu dát. Intervencia človeka je minimálna.



\section{Chatboty historické a dnešné}

Od čias, keď vznikli počítače, bola túžba ľudí po čo najjednoduchšej komunikácii s nimi bola neprehliadnuteľná. Samotný Alan Turing sníval o tom, že raz budú počítače tak inteligentné, že sa vyrovnajú intelektu človeka a človek sám nebude schopný rozpoznať či komunikuje so strojom alebo počítačom. O tom svedčí jeho článok z 1950, kde popísal tzv. imitačnú hru, neskôr známu ako turingov test (umelej inteligencie). \cite{Turing1950a}

\subsection{Chatbot založený na pravidlách}

Pravidlá môžu mať rôznu podobu, ako napríklad práve šablónovanie a použitie regulárnych výrazov ale aj založené na vyhľadávaní kľúčových slov z užívateľského vstupu. Pomocou týchto pravidiel tvoria odpovede podľa predpísaného scenáru, podobne ako sa herec drží svojho scenáru.

Uveďme príklad, kedy užívateľ chce zmeniť svoj pin-kód na bankomatovej karte. Možný vstup by mohol byť \uv{Prosím, chcem si \textit{zmeniť} svoj \textit{pin} na \textit{karte}.}, kde stroj -- chatbot -- by podľa výskytu kľúčových slov v texte (povedzme: \uv{zmeniť}, \uv{pin}, \uv{karte})

Prvý pokus o podobný stroj bol vyvinutý v laboratóriách MIT Josephom Weizenbaumom, ktorý dostal názov ELIZA. \cite{Weizenbaum1966} Pomocou šablón a regulárnych výrazov -- tzv. pattern matching -- mala demonštrovať schopnosť komunikácie stroja s človekom. Radí sa teda medzi prvé chatboty (v tom čase nazývané tzv. \uv{chatterboty})

Turingov test bol však nahradený mnohými novými spôsobmi merania \uv{inteligencie} stroja, ako napríklad čínska miestnosť \cite{Searle1980}, ktorá však bola kritizovaná \cite{Hauser1997} ale aj podporovaná \cite{sep-chinese-room}.

\subsection{Dnešné účelové chatboty}

Dnes sa chatboty vyznačujú hlavne tým, že plnia priania a príkazy užívateľov, a teda výsledkom \uv{konverzácie} užívateľa a stroja je splnená požiadavka na vykonanie nejakého špecifického úkonu. Oproti ELIZE alebo iných moderných čisto konverzačných chatbotov sa líši práve tým, že poskytnú riešenie nejakého problému a užívateľovi takýmto spôsobom vedia uľahčiť alebo aj zjednodušiť prácu. Môže ísť o textové alebo hlasové ovládanie telefónu, inteligentnej domácnosti alebo aj internetového bankovníctva. Existujú aj chatboty určené čisto na konverzáciu s užívateľom, avšak v tejto práci sa nimi nezaoberáme.

Medzi dnešné -- moderné -- konverzačné systémy môžeme zaradiť napríklad Siri od spoločnosti Apple \cite{AppleInc.2011} pomocou ktorej vie užívateľ ovládať (nielen) hlasom svoj telefón, Google Assistant \cite{Google2019} ktorý dnes vie užívateľovi napríklad rezervovať termín u kaderníka prostredníctvom automatického telefonického hovoru alebo Erica \cite{BankofAmerica} -- virtuálny bankový asistent, pomocou ktorého vie klient banky odoslať peniaze na iný účet, zobraziť informácie o pohyboch na konte a množstvo iných funkcií (Erica sa dá ovládať aj hlasom).

Najdôležitejšia súčasť akéhokoľvek účelového chatbota je tzv. detekcia zámeru (úmyslu, intentu) užívateľa. Od toho sa neskôr odvíja samotná konverzácia, prípadne vykonanie príkazu alebo práce.



\section{Intent detection\label{sec:intent}}

Alebo detekcia kontextov, zámerov, úmyslov (angl. \textit{\uv{intent detection}}). Jedná sa o problém klasifikácie textu (dokumentu) do $\bm{c}$ tried (intentov). Tento problém je v mnohých ohľadoch veľkou výzvou \cite{Wen2017}, hlavne preto, že na úspešnosti tohoto \uv{odhaľovania} zámeru/príkazu užívateľa závisí celá nasledujúca konverzácia užívateľa s chatbotom. Ak systém detekcie zlyhá, je to prvý kontaktný bod s užívateľom daného systému a môže ho to odradiť od ďalšieho používania. \cite{Casanueva2020}\\

Problém je definovaný nasledovne: Majme množinu $X$, kde každý prvok je sekvencia znakov rôzne dlhá (pri intent detection je to jedna alebo viac viet v prirodzenom jazyku). Ďalej máme k dispozícii vektor $Y$, kde každý prvok je číselné priradenie do správnej triedy. Jedná sa teda o supervizované učenie, klasifikačný problém viacerých tried (bližšie v sekcii \ref{sec:supervised}).\\

Odkedy začali byť počítače dostupnejšie, vznikali rôzne dátové sady určené na detekciu intentu, napríklad pre rezerváciu letu \cite{hemphill-etal-1990-atis} alebo získanie informácie o verejnej doprave \cite{Raux2005}. Dát pre tréning akéhokoľvek moderného modelu dnes stále nie je dosť, takže tento problém často naberá scenár tzv. few-shot learningu, kde k dispozícii je len obmedzený počet vzorkov pre intent \cite{Casanueva2020} \cite{Le2020}. V kontexte konverzačných systémov je intent detection spájaný dohromady aj s tzv. rozpoznávaním entít v texte (angl. entity recognition) \cite{Lorenc2021} \cite{Le2020} \cite{Wang}. 

Niektoré výskumy sa sústreďujú priamo na jednotlivé moduly detekcie intentu (konkrétne transformery a modely podobné BERTovi, viď. sekciu \ref{sec:tfmr}) a skúšajú vylepšiť tieto modely pomocou rôznych techník pre prevedenie few-shot learningu do praxe \cite{Mehri2021}. V ďalších prácach sa pokúšajú riešiť úplnú absenciu dát pomocou tzv. zero-shot learningu, kde len na základe názvov intentov chcú daný vstup od užívateľa správne klasifikovať \cite{Xue2021}.

\subsection{Vhodné dáta}

V tejto práci sa zaoberáme hlavne získaním kvalitných a reálnych dát od užívateľov, a to špecificky pre doménu bankovníctva. Pokúšame sa vytvoriť scenár few-shot learningu a riešiť tento problém aj s menším počtom vzoriek (priemerne cca. 100 vzorkov pre intent/triedu). Vhodné modely na tento scenár sú práve predtrénované siete podobné BERTovi \cite{Devlin2018}, so síce veľkým počtom parametrov, avšak so silnou štatistickou znalosťou jazyka. Ladenie takýchto modelov dosahuje dobré výsledky a je jednoduchšie ako tréning veľkého modelu odznova.

Existuje iba niekoľko dátových sád pre intent detection, ako napríklad HWU64 \cite{Liu2019}, kde sa nachádza 64 rôznych tried (zámerov), z 21 rôznych domén s počtom vzorkov 25 716. Ďalšia dátová sada hodná spomenutia pomenovaná CLINC150 \cite{larson-etal-2019-evaluation} obsahuje 150 tried (zámerov) z 10 domén s počtom vzoriek 23 700. Tieto dátové sady sú však len ťažko schopné pokryť šírku domén, ktorých sa dotýkajú \cite{Casanueva2020} a takisto sa nešpecializujú na doménu bankovníctva, takže použité byť nemohli. Ďalšou veľkou prekážkou pri budovaní akéhokoľvek účelového konverzačného systému orientovaného na jednu doménu je pri jazykoch iných ako angličtina práve nedostupnosť dát pre tvorbu akéhokoľvek systému \cite{Casanueva2020}.

Vytvorili sme preto spolu s kolegami vo firme Profinit vlastnú dátovú sadu pomocou Text2Bank aplikácie. Túto sadu sme nazvali TEXT2BANK13. Keďže táto sada má malý počet vzoriek a aj intentov -- tried, bolo nutné ju obohatiť o ďalšie vzorky. Preto sme použili verejne dostupnú dátovú sadu BANKING77 \cite{Casanueva2020}, ktorú sme strojovo preložili a po predspracovaní zlúčili s TEXT2BANK13 pre vytvorenie sady vhodnej pre strojové učenie a demonštráciu finálnej verzie aplikácie Text2Bank. Tieto dáta sú popísané v praktickej časti tejto práce.



\chapter{Aloritmy strojového učenia}

V tejto kapitole popíšeme typy a konkrétne implementácie niektorých algoritmov strojového učenia (angl. machine learning).

\section{Typy strojového učenia}

Strojové učenie môžeme rozdeliť na štyri hlavné disciplíny \cite{Flach2012} \cite{Ayodele2010}:

\subsection*{Supervizované učenie\label{sec:supervised}}
Je taktiež nazývané tzv. strojové učenie s učiteľom. Na základe historických dát -- príznakov -- a k nim pridruženým vektorom príslušností príznaku do danej triedy -- máme za úlohu predikovať nové príznaky do správnej triedy.

Modelový príklad pre vysvetlenie: máme k dispozícií informácie o pacientoch, u ktorých sme našli príznaky chrípky a zároveň lekári pacientov vyšetrili, pozorovali a u každého určili, či chrípku mal alebo nemal. Máme teda zoznam "príznakov" (angl. features) a ku príznakom každého pacienta máme k dispozícii jeho finálny stav (mal alebo nemal chrípku). Supervizované učenie v tomto prípade znamená, že vytvoríme tzv. model, ktorý na základe predošlých dát určí, či nový pacient s iným zoznamom príznakov má alebo nemá chrípku a s akou pravdepodobnosťou. Tento modelový príklad zaradíme do problémov binárnej klasifikácie. Na predikciu môžeme použiť napríklad model logistickej regresie. \cite{Tolles2016}

V kontexte spracovania prirodzenej reči tu radíme spomínaný intent detection, ktorý sa prevádza na problém klasifikácie textu, a je popísaný v sekcii \ref{sec:intent}.

\subsection*{Nesupervizované učenie} 
Podobne ako pri supervizovanom učení máme k dispozícii príznaky, avšak vektor príslušnosti príznaku tu úplne chýba. V tomto prípade chceme nájsť v dátach štruktúru pomocou štatistických alebo iných metód, prípadne porozumieť dátam a vhodne ich popísať. Tento typ sa bežne voláme strojové učenie bez učiteľa.

\subsection*{Semi-supervizované učenie}
V tomto type máme k dispozícii veľké množstvo príznakov bez príslušnosti do danej triedy, ale k tomu len relatívne malé množstvo dát, kde poznáme triedu, do ktorej daný príznak patrí. Tento typ učenia je náročnejší, práve kvôli nedostatku labelovaných dát.

\subsection*{Reinforcement learning}
Alebo tzv. posilňovacie učenie používa pozorovania zozbierané z interakcii s prostredím, a pomocou fitness funkcie sa algoritmus učí sekvenčne. Chýba tu vektor príslušnosti (na rozdiel od supervizovaného učenia) a zároveň model vie získavať spätnú väzbu z prostredia (na rozdiel od nesupervizovaného).


\section{Naive Bayes classifier}

Metódy naivného bayesa (angl. \gls{nb}) sú založené na aplikácii bayesovskej vety s naivným predpokladom nezávislosti príznakov $X = (X_1, \dots, X_p)^T$ s podmienkou vysvetľovanej premennej $Y=y$. T.j. $\forall y\in \mathcal{Y}$ a $\bm{x} \in (x_1,\dots,x_p)^T \in \mathcal{X}$ platí:

$$
\mathrm{P}(\bm{X} = \bm{x}|Y = y) = \mathrm{P}(X_1 = x_1|Y = y) \cdot,\dots,\cdot \mathrm{P}(X_p = x_p|Y = y).
$$
Naivita znamená, že pre fixnú hodnotu vysvetľovanej premennej predpokladáme, že sú príznaky nezávislé. Výsledný \gls{map} odhad (alebo predikcia $\hat{Y}$) bayesovského klasifikátora je teda:
$$
\hat{Y} = \arg \max_{y\in Y} \prod_{i=1}^{p} \mathrm{P}(X_i = x_i|Y = y) \mathrm{P}(Y = y).
$$
Ak je daný príznak $X$ spojitou náhodnou veličinou, tak sa namiesto podmienenej pravdepodobnosti pre tento príznak vezme podmienená hustota pravdepodobnosti $f_{X|y}(x)$, čo je hustota pravdepodobnosti veličiny X podmienená javom $Y = y$ a odpovedá distribučnej funkcii $F_{X|y}(x) = \mathrm{P}(X_i \leq x_i|Y = y)$. Predikciu \gls{map} odhadom prevedieme pomocou vzťahu
$$
\hat{Y} = \arg \max_{y\in Y}
\prod_{i=1}^{l} \mathrm{P}(X_i = x_i|Y = y)
\prod_{i=l+1}^{p} f_{X_i|y}(x_i) \mathrm{P}(Y = y),
$$
kde $X_1, \dots, X_l$ sú diskrétne príznaky a $X_{l+1}, \dots, X_p$ sú príznaky spojité. 
Tento klasifikátor je bežne používaný na klasifikáciu dokumentov a textu (napr. spam detection) pomocou tzv. bag-of-words, avšak nahradili ho inteligentnejšie spôsoby klasifikácie. My tento model v tejto práci trochu ohneme a používame práve na zber dát, pretože jeho hlavný prínos je ľahká interpretovateľnosť výsledkov predikcie. \cite{ArelKloudaJuanPabloMaldonadoLopez2022} \cite{Zhang}

Častým modelom podmieneného rozdelenia je v spojitom prípade normálne rozdelenie $N(\mu_y, \sigma_y^2)$ so strednou hodnotou určenou parametrom $\mu_y$ a rozptylom $\sigma_y^2$. Podmienená hustota je teda $\forall y\in \mathcal{Y}$
$$
f_{X|y}(x) = 
\frac{1}{\sqrt{2\pi\sigma^2_y}} 
\exp\left(-\frac{(x - \mu_y)^2}{2\sigma^2_y}\right)
$$
a obvykle sa používajú \gls{mle} odhady na výpočet $\hat{\mu}$ a $\hat{\sigma_y^2}$. Implementácia v knižnici \verb*|scikit-learn| je dostupná v balíčku \verb*|GaussianNB|. \cite{sklearn_api} \cite{ArelKloudaJuanPabloMaldonadoLopez2022} \cite{Zhang}




\section{KMeans clustering}

Keď hovoríme o zhlukovaní (angl. clustering), myslíme úlohu nesupervizovaného učenia. Algoritmus KMeans funguje iteratívne: Najprv inicializujeme model tak, že rozmiestnime $k$ stredových bodov $\mu_1, \dots, \mu_k$ do priestoru z $\mathbb{R}^p$, kde chceme nájsť $k$ zhlukov. Iteratívne potom: 
\begin{enumerate}
	\item 
	roztriedime body do zhlukov $C_i = \{x \in D | i = \arg \min_j || x - \mu_j|| \}$,
	\item 
	prepočítame body $\mu_1, \dots, \mu_k$ ako geometrické stredy týchto zhlukov: $\mu_i \rightarrow \frac{1}{|C_i|}\sum_{x\in C_i} x$.
\end{enumerate}
Beh algoritmu zastavíme, akonáhle je zmena hodnoty účelovej funkcie 
$$
G(C) = \sum_{i=1}^{k} \frac{1}{2|C_i|} \sum_{x,y \in C_i}(||x_i - \mu_j||^2)
$$
medzi jednotlivými iteráciami dostatočne malá. Implementácia bola použitá z knižnice \verb*|sklearn|. \cite{KloudaKarelKovalenko2022} \cite{sklearn_api}

KMeans sme v tejto práci použili pri zlučovaní dátových sád a pri analýze jednotlivých vzoriek intentov.

\section{UMAP}

Alebo angl. Uniform Manifold Approximation and Projection (jednotná priestorová aproximácia a projekcia) je \textbf{technika redukcie dimenzionality}, ktorú môžeme použiť podobne ako t-SNE \cite{VanDerMaaten2008}, ale aj všeobecne pre nelineárnu redukciu dimenzie. Algoritmus funguje na troch rôznych predpokladoch o dátach:
\begin{enumerate}
	\item 
	dáta sú uniformne rozdelené na riemannovom priestore,
	\item 
	riemannova metrika je lokálne konštantná (môže byť aproximovaná),
	\item 
	priestor je lokálne prepojený.
\end{enumerate}
Z týchto predpokladov je možné modelovať priestor s fuzzy topologickou štruktúrou. Pre dáta s nízkodimenzionálnou projekciou vie UMAP nájsť embeddingy dát, ktoré majú najbližšiu možnú fuzzy topologickú štruktúru.
\cite{McInnes2018}

Redukciu dimenzie v tejto práci používame hlavne na vizualizáciu viac dim. priestorov do 2 dimenzií, kde sa dáta ľahšie interpretujú.

\section{Neurónové siete}

\begin{figure}[t]
	\centering
	\includegraphics[width=0.7\textwidth]{pictures/NN.png}
	\caption{Zjednodušená ukážka neurónovej siete \cite{Muran2019}}
	\label{fig:NN}
\end{figure}

Neurónová sieť sa snaží simulovať to, čo veríme, že robí náš mozog. Samozrejme, že sú s tým spojené mnohé limitácie a nepreskúmané možnosti. Proces učenia simulujeme pomocou matice váh $W$, ktorá reprezentuje jednu vrstvu v neurónovej sieti. Týchto vrstiev môže byť v neurónovej sieti viac za sebou tak, aby sme získali robustný model. Vrstvy za vstupnou nazývame skryté. Základná rovnica pre jednu vrstvu siete je
$$
o_l = f(W_li_l+b_l),
$$
kde $o_l$ je výstupný vektor na $l$-tej vrstve dimenzie $R^{o\times1}$, $i_l$ je vstupný vektor dim. $R^{i\times1}$, $f$ je nelineárna aktivačná funkcia $b_l$ je bias pre $l$-tú vrstvu a $W_l$ je matica váh tejto vrstvy. Proces učenia je v tomto prípade určenie hodnôt matice $W$. Zjednodušenú verziu siete môžeme vidieť na obrázku \ref{fig:NN}.
Zvyčajne sa váhy určia pomocou algoritmu spätnej propagácie. \cite{SAZLI2006}

\subsection{Rekurentné Neurónové siete}

Rekurentné siete (angl. \gls{rnn}) sú špeciálne typy sietí, ktoré si vedia dobre poradiť so sekvenčnými dátami. Jej vstupom je sekvencia $\left(x_{1}, x_{2}, \ldots, x_{N}\right)$ a výstupom je sekvencia skrytých stavov $\left(h_{1}, h_{2}, \ldots, h_{N}\right)$, kde
$$
h_{t}=f\left(W_{x} x_{t}+W_{h} h_{t-1}+b_{n}\right).
$$
Skrytý stav reprezentuje kus špecifickej informácie o vstupe. Tento klasický prístup \gls{rnn} však nie je schopný si pamätať dlhšie závislosti v sekvenciách. Preto sa používa vylepšená verzia, tzv. \gls{lstm}. Pridaná \uv{pamäťová} bunka, ktorá sa tu nachádza, lepšie modeluje dlhšie závislosti. Klasická architektúra \gls{lstm} je zložená z: \cite{SAZLI2006} 

\begin{itemize}
	\item 
	vstupnej brány: $i_{t}=f\left(W_{i} x_{t}+W_{i} h_{t-1}+b_{i}\right)$,
	\item 
	zabúdacej brány: $f_{t}=f\left(W_{f} x_{t}+W_{f} h_{t-1}+b_{f}\right)$,
\item 
	výstupnej brány: $o_t = f\left(W_{o} x_{t}+W_{o} h_{t-1}+b_{o}\right)$,
\item 
	vnútorného stavu: $ u_{t}=\tanh \left(W_{u} x_{t}+W_{u} h_{t-1}+b_{u}\right)$,
	\item 
	pamäťovej bunky: $c_{t}=i_{t} \times u_{t}+f_{t} \times c_{t-1}$.
\end{itemize}
Násobenie po prvkoch je značené ako $\times$, $x_t$ je vstupný vektor o veľkosti $d$ v čase $t$ a $W$ je matica váh, ktoré sa sieť musí naučiť. Učenie prebieha pomocou gradientného zostupu. Základnú architektúru modelu môžeme vidieť na obrázku \ref{fig:LSTM}. \cite{Olah2015} \cite{SAZLI2006} 

\begin{figure}[t]
	\centering
	\includegraphics[width=0.9\textwidth]{pictures/LSTM.png}
	\caption{Opakujúci sa modul v \gls{lstm} so štyrmi navzájom prepojenými vrstvami. \cite{Olah2015}}
	\label{fig:LSTM}
\end{figure}

\subsection{Obojsmerné LSTM (BiLSTM)}

Obyčajné \gls{lstm} majú svoje limity a stále neboli dostatočne dobré pre spracovanie prirodzenej reči. Myšlienka obojsmerného spracovania priniesla možnosť tréningu za použitia všetkých minulých a budúcich častí vstupnej sekvencie, a to v zadanom časovom okne. V skratke sa dá povedať, že BiLSTM sú dve nezávislé \gls{lstm} siete, avšak každá sa pozerá do kontextu okolia slova v čase $t$ opačným smerom (dopredu a dozadu) a teda sú trénované nezávisle na sebe (pomocou back propagation). Výstup v čase $t$ je pripojením $h_t$ a $h_t'$. Počet parametrov siete je teda oproti \gls{lstm} dvojnásobný. \cite{Chiu}



\section{Reprezentácia slov a viet vo vektorovom priestore}

Populárny prístup k modelovaniu jazyka je transformácia slov do vektorov. Vytvorené vektory v sebe držia skryté informácie o reči, ako sú analógie alebo sémantika. Ak by sme chceli takéto vektory (embeddingy) vytvoriť, potrebujeme na to veľký obnos dát -- tzv. korpus (napríklad Wikipedia \cite{Wikimedia2013}), kde sa nachádzajú milióny slov v rôznych kontextoch (a jazykov, takže sa dá modelovať každý jazyk zvlášť). Jedna z najpopulárnejších techník sa nazýva Word2Vec. \cite{Mikolov}

\subsection{Word2Vec}

FastText dodáva dva modely pre výpočet slovných embeddingov (vektorov, reprezentácií): tzv. skipgram a \gls{cbow}. Skipgram sa učí predikovať cieľové slovo pomocou slov v jeho okolí a naopak \gls{cbow} predikuje cieľové slovo pomocou kontextu -- okolia. Tento kontext je reprezentovaný ako množina slov vo fixne veľkom \uv{okne} okolo cieľového slova. \footnote{Problém predikcie cieľového slova je modelovaný ako množina nezávislých binárnych klasifikácií} skipgram sa viac hodí pre menšie korpusy a je schopný dobre reprezentovať vzácne slová, kdežto \gls{cbow} sa rýchlejšie trénuje (vhodnejší pre veľké korpusy) a lepšie reprezentuje frekventované slová. Každé slovo z korpusu predtým rozdelíme na tzv. n-gramy, podslová. \footnote{napr. pre $n=3$ slovo $ \mathrm{kdepak} = \{ <\mathrm{kd}, \mathrm{kde}, \mathrm{dep}, \mathrm{epa}, \mathrm{pak}, \mathrm{ak}>, <\mathrm{kdepak}> \} $ kde $< >$ sú špeciálne znaky začiatku a konca slova} Pre učenie reprezentácií na skrytej vrstve tohto modelu sa používa stochastický gradientný zostup, kde minimalizujeme

$$
\sum_{t=1}^{T}\left[\sum_{c \in \mathcal{C}_{t}} \ell\left(s\left(w_{t}, w_{c}\right)\right)+\sum_{n \in \mathcal{N}_{t, c}} \ell\left(-s\left(w_{t}, n\right)\right)\right],
$$
kde $T$ je počet slov v korpuse (sekvencia slov $w_1,\dots,w_T$), $C_t$ sú indexy slov okolo $w_t$, $l(.) x \rightarrow \log (1+e^{-x})$ je stratová logaritmická funkcia, $s : (w, w_c) \rightarrow R$ je skórovacia funkcia pre slovo a jeho kontext a $\mathcal{N}_{t,c}$ sú náhodné negatívne vzorky kontextu c. Takýmto spôsobom vieme pomocou rekurentnej neurónovej siete získať reprezentácie slov -- embeddingy -- z jej skrytej vrstvy. \cite{Mikolov} 

\subsection{FastText}

Je knižnica, ktorá používa model word2vec s n-gramami a špeciálnymi symbolmi pre začiatok a koniec slova pre rozlišovanie predpôn a prípon. Veľkosť slovníka je nastavená na $K=2.10^{6}$ a každé slovo je reprezentované indexom v slovníku a množinou n-gramov.
K dispozícii sú predtrénované modely na Wikipédii a Common Crawl korpuse v požadovanom jazyku (v našom prípade čeština a angličtina) \cite{Grave2019}. Použitý bol model \gls{cbow} s $n=5$, dimenziou skrytej vrstvy $d=300$ (a teda aj veľkosť embeddingov je 300) a s oknom veľkosti 5 slov. \cite{Mikolov2019} Tieto reprezentácie -- embeddingy -- sa získavajú pomocou trénovania rekurentnej neurónovej siete s 650 \gls{lstm} jednotkami \cite{Bojanowski}.

FastText vieme použiť aj na získanie tzv. vetných embeddingov. Po určení slovných embeddingov z vety a po normalizácii ich vektorov vieme pomocou priemeru všetkých vektorov získať nový -- vetný embedding (v rovnakom priestore, ktorý bol modelovaný slovnými embeddingami). \cite{Bojanowski}

\subsection{LASER}

Tým, že je bolo modelovanie jazyka od uvedenia word2vec spopularizované, boli vytvorené mnohé ďalšie nástroje pre reprezentáciu slov a viet prirodzeného jazyka. Pre potrebu tvorby embeddingov viet rôznych jazykov tak, aby sa výsledné vektory v priestore nachádzali blízko seba bol vyvinutý LASER -- Language Agnostic SEntence Representations. Tento nástroj z dielne Facebooku vie reprezentovať vety z viac ako 90 jazykov a 28 rôznych znakových sád. \cite{Artetxe}\\

\begin{figure}[t]
	\centering
	\includegraphics[width=0.9\textwidth]{pictures/LASER.png}
	\caption{Architektúra LASERu pre učenie viacjazyčných embeddingov \cite{Artetxe}}
	\label{fig:LASER}
\end{figure}

Architektúra tohto nástroja je enkodér-dekodér, kde enkodér je zložený z 5 vrstiev BiLSTM \cite{Chiu}. LASER funguje na princípe podobnom, ako tzv. \uv{sequence-to-sequence} translácia, avšak chýba tu attention vrstva, ktorá je nahradená vektorom o fixnej veľkosti 1024. Tento vektor reprezentuje vstupnú sekvenciu. Embeddingy sú potom získavané aplikáciou operácie max-poolingu z tejto poslednej skrytej vrstvy enkodéra. Túto architektúru môžeme vidieť na obrázku \ref{fig:LASER}. Keďže jazyk sekvencie nie je indikovaný pre enkodér ale len pre dekodér, enkodér je nútený sekvencie ukladať na približne podobné miesto v priestore embeddingov. \cite{Artetxe}



\section{Transformery\label{sec:tfmr}}


\begin{figure}[t]
	\centering
	\includegraphics[width=0.4\textwidth]{pictures/transformer.png}
	\caption{Architektúra transformeru, naľavo enkodér pre vstupy, napravo dekodér pre výstupy (N krát za sebou). Podvrstvy každej z týchto dvoch častí sú pozornosť (attention) a dopredná neurónová sieť nasledujúca hneď za ňou.}
	\label{fig:tfmr}
\end{figure}

V roku 2017 bola po prvýkrát popísaná architektúra takzvaného transformeru. Výskum sa vtedy zameriaval na strojový preklad a táto architektúra poskytla na tú dobu najlepšie výsledky pri strojovom preklade. \cite{Vaswani2017a} \\
Kľúčový komponent transformerov je tzv. \uv{vrstva pozornosti} (angl. attention layer). Tieto vrstvy \uv{napovedajú} celému modelu, na ktoré slová v sekvencii má upriamiť pozornosť a ktoré slová si môže dovoliť \uv{ignorovať}. Táto architektúra nahradila doterajšie prístupy pomocou konvolučných neurónových sietí a paralelizáciou zrýchlila trénovanie. Architektúra sa skladá z enkodéru a dekodéru, pretože táto architektúra bola v prvom rade určená pre strojový preklad. Môžeme ju vidieť na obrázku \ref{fig:tfmr}. \cite{Vaswani2017a}\\

Enkodér mapuje vstupné sekvencie reprezentácií symbolov (tokenov) $(x_1, \dots, x_n)$ do sekvencií súvislých reprezentácií $\mathbf{z} = (z_1, \dots z_n)$. Zo $\mathbf{z}$, dekodér generuje výstupné sekvencie $(y_1, \dots, y_n)$ symbolov (tokenov) jeden prvok za jednu časovú jednotku. Transformer je teda zložený zo zoskupení modulov pozorností a bodových, plne prepojených neurónových sietí ako pre enkodér, tak pre dekodér. Zobrazené sú na ľavej a pravej polke na obrázku \ref{fig:tfmr}. \cite{Vaswani2017a}




\subsection{Attention -- pozornosť}

\begin{figure}[t]
	\centering
	\includegraphics[width=0.4\textwidth]{pictures/attention.png}
	\caption{Viac ($h$) hlavová pozornosť. \uv{Linear} je jedna z 3 sietí \uv{Scaled dot product attention} je definovaná v rovnici \ref{eq:att} a \uv{Concat} je operácia pripojenia výsledných vektorov z $h$ hláv za seba. \cite{Vaswani2017a}}
	\label{fig:attention}
\end{figure}

Na začiatku zo všetkých slov na vstupe vytvoríme embeddingy tokenov (pozícia v slovníku po tokenizácii). Na obrázku \ref{fig:tfmr} vidíme celú architektúru, kde sa vstupná sekvencia pozičných embeddingov (pre zachovanie informácie o poradí tokenov zo vstupu) dostáva do vrstvy viac-hlavovej pozornosti. Pomocou troch neurónových sietí sa zo vstupných embeddingov (tokenov) vytvoria tri vektory: query $Q$, key $K$, a value $V$. Pomocou siete chceme nájsť podobnosť $Q$ s $K_i$ vektormi ostatných tokenov. Škálovaným skalárnym súčinom vektoru $Q$ s ostatnými $K_i$ dostaneme ohodnotenie (skóre) každého nasledujúceho vstupného embeddingu. Takto model vie, na ktoré nasledujúce tokeny má upriamiť pozornosť (čím vyššie skóre, tým vyššia pozornosť pre daný embedding). Výstupom z jednej hlavy pozornosti je 
\[
\mathrm{Attention}(Q,K,V) = \mathrm{softmax}(\frac{QK^T}{\sqrt{d_k}})V, \label{eq:att} \tag{$1$}
\] 
kde $d_k$ je dimenzia $K$. Softmax je použitý pre normalizáciu výsledných skóre každého tokenu. \cite{Vaswani2017a}

Takýchto hláv je viacero a každý výstup hlavy je pripojený za seba do jedného spoločného vektoru, ktorý je podhodený neurónovej sieti. Jej výstup ide do reziduálneho pripojenia vrstvovej normalizácie. \cite{Rush2019} \cite{He2016}.

\subsection{Transfer learning a fine-tuning v NLP}

\begin{figure}[t]
	\centering
	\includegraphics[width=0.9\textwidth]{pictures/pretrain.png}
	\caption{Predtrénovací postup, zjednodušene. \cite{Wolf2019}}
	\label{fig:pretrain}
\end{figure}

Aj vďaka uvedeniu transformer architektúry -- hlavne BERTa \cite{Devlin2018} a GPT \cite{Openai} bolo umožnené veľmi efektívne spracovanie prirodzenej reči. Táto transformer architektúra bola určená na tvorbu predtrénovaných modelov, ktoré pomocou prevodného učenia (angl. transfer learningu) bolo možné použiť na riešenie mnohých problémov \gls{nlp} pomocou vyladenia (angl. fine-tuning) predtrénovaného modelu. Dovtedy boli úlohy \gls{nlp} veľkou výzvou a vyžadovali časovo a zdrojovo náročné trénovania modelov. \cite{Wolf2019}

Predtrénovanie prebieha na veľkom množstve dát so zámerom dobre zachytiť štatistické porozumenie jazyka. Model sa inicializuje s náhodnými váhami bez žiadnej predošlej znalosti dát. Zjednodušený postup predtrénovania môžeme vidieť na obrázku \ref{fig:pretrain}. Dôvody, prečo je použitie predtrénovaných modelov výhodné:

\begin{itemize}
	\item 
	Predtrénovaný model už bol natrénovaný na dátach, ktoré sú podobné úlohe, ktorú v \gls{nlp} chceme riešiť. Model teda získanú znalosť môže využiť neskôr pri ladení.
	\item 
	Predtrénovaný model je možné opätovne použiť a vyladiť na riešenie konkrétnej úlohy (odlišnej od úlohy v predtrénovacej časti) s vyššou šancou na získanie rozumných výsledkov.
	\item 
	Tréning týchto modelov typicky trvá týždne, stojí veľa peňazí a zdrojov.
	\item 
	Ladenie je preto menej náročné a vyžaduje skôr hodiny ako týždne na tréning.
\end{itemize}
Ladenie predtrénovaného modelu má teda nižší časový, finančný, dátový a aj environmentálny dopad. Ak teda nemáme k dispozícii dostatok vhodných dát, je rozumnejšie opätovne použiť hotový model s nadobudnutými znalosťami pre špecifickú úlohu, ktorú riešime. \cite{Wolf2019} \cite{Devlin2018} \cite{Jordan2001} \cite{Howard2018}


\subsection{BERT}

Hlavnou úlohou tohoto modelu je možnosť opätovného použitia hotovej siete pre rôzne úlohy \gls{nlp}. Predtrénovanie tohoto modelu prebieha na neoznačených (nelabelovaných) štrukturovaných dátach -- dokumentoch, takže nie je nutné skoro žiadne predspracovanie. Vstupné sekvencie sú tokenizované pomocou Wordpiece algoritmu \cite{Wu}. Architektúra BERTa je viacvrstvový obojsmerný transformer enkodér popísaný v sekcii \ref{sec:tfmr}. Z toho vznikla skratka BERT (angl. Bidirectional Encoder Representations from Transformers). \cite{Devlin2018}

\subsubsection*{\gls{mlm}}

Alebo maskované modelovanie jazyka, na ktoré sa bežne odkazuje ako na tzv. Cloze úlohu doplnenia chýbajúceho slova do vety \cite{Taylor}. V praxi sa náhodne nahradia tokeny v sekvenciách (vetách) vstupného textu špeciálnym tokenom \verb*|[MASK]|. Model sa snaží tieto zamaskované tokeny predikovať správne. V základnom BERT modeli to bolo 15\% všetkých tokenov. Aby bola zaručená schopnosť následného ladenia (pretože sa \verb*|[MASK]| token pri následnom ladení nevyskytuje), spomínaných 15\% náhodne vybraných tokenov z celého korpusu je z (1) 80\% nahradených špeciálnym tokenom \verb*|[MASK]|, (2) 10\% náhodným tokenom a zvyšných (3) 10\% nezmeneným. Posledný skrytý vektor tohto tokenu je použitý na jeho predikciu so stratovou funkciou krížovej entropie. \cite{Devlin2018}

\subsubsection*{\gls{nsp}}

Alebo predikcia nasledujúcej vety je úloha, ktorá zaručí \uv{porozumenie} vzťahu medzi dvoma vetami výsledného modelu, ktoré nie je priamo zachytený pomocou modelovania jazyka. Preto je tento predtrénovaný model vhodný použiť na úlohy odpovedania na otázky a dedukciu z textu. \\
\gls{nsp} je prevedené na úlohu binárnej klasifikácie, kde pre trénovacie páry viet $A$ a $B$ označia polovicu viet ako nasledujúcu a pre zvyšnú polovicu párov nahradia nasledujúcu vetu náhodne vybranou a označia ako nenasledujúcu. Model sa počas tréningu snaží túto úlohu vyriešiť a tvorcovia tvrdia, že trénovanie na tejto úlohe výrazne pomáha pri vyššie spomínaných úlohách odpovedania na otázky a dedukciu. \cite{Devlin2018}


\subsubsection*{Architektúra a tréning}

\begin{figure}[t]
	\centering
	\begin{subfigure}[b]{0.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{pictures/BERTpretrain.png}
		\caption{Predtrénovanie BERTa \cite{Devlin2018}}
		\label{fig:BERTpre}
	\end{subfigure}
	\hfill
	\begin{subfigure}[b]{0.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{pictures/BERTclf.png}
		\caption{BERT pre klasifikáciu textu \cite{Devlin2018}}
		\label{fig:BERTclf}
	\end{subfigure}
	
	\caption{Naľavo: spôsob predtrénovania celého obojsmerného modelu BERTa. Napravo: BERT model určený ladenie pre úlohu klasifikácie textu.}
	
\end{figure}

Na obrázku \ref{fig:BERTpre} vidíme ukážku architektúry BERTa, kde sú na vstupe páry viet $A$ a $B$. V každej vete sa môže nachádzať zamaskovaný token. Vety sú oddelené špeciálnym tokenom \verb*|[SEP]| pre oddelenie kontextu a na začiatku každého páru je pridaný špeciálny token \verb*|[CLF]| a jeho výstup je práve predikcia nasledujúcej vety. Pri ladení sa tento token používa na výstup akejkoľvek klasifikácie, viditeľné na obrázku \ref{fig:BERTclf}. Tieto tokeny sú prevedené na ich embedding reprezentáciu a následne spracované obojsmernými enkodér časťami transformeru vďaka krížovej pozornosti (modrá časť diagramu).

Ak chceme model použiť, napríklad, na úlohu intent detection -- klasifikáciu textu, všetky výstupy skrytej vrstvy $Ti$ \uv{ignorujeme} a ponecháme len výstup klasifikačnej vrstvy $C$. \cite{Peltarion} \cite{Devlin2018}



\section{Použité predtrénované modely}

V tabuľke \ref{tab:models} vidíme popis jednotlivých predtrénovaných modelov. Použité dáta sú národný český korpus (Nat) \cite{Kren2016}, texty Wikipédie (v češtine) \cite{Wikimedia2013}, Stiahnuté články z českých médií a Czech Colossal Clean Crawled Corpus (C5) \cite{Raffel2020}, czes \cite{11858/00-097C-0000-0001-CCCF-C} a W2C \cite{11858/00-097C-0000-0022-6133-9}.

Všetky tieto predtrénované siete sú vďaka \url{Huggingface.co} hubu \cite{Wolf2019} verejne dostupné na ich stránkach. Nachádza sa tu veľké množstvo predtrénovaných modelov. Modely trénované na vyššie spomenutých českých dátach sú vypísané viditeľné na tabuľke \ref{tab:models}.


\begin{table}[h]
	\centering
	\begin{tabular}{llllll}
		\hline
		meno & Arch. & Vocab & Dáta & \#param \\ \hline
		CZERT-B & BERT  & 40 tis. & Nat+Wiki+News, 37GB & 109 M \\
		CZERT-A & ALBERT  & 40 tis. & Nat+Wiki+News, 37GB & 12 M \\ 
		FERNET & BERT & 100 tis. & C5, 93GB & 164 M \\ 
		Robe-Czech & RoBERTa  & 52 tis. & Nat+Wiki+Czes+W2C & 125 M \\ 
		Slavic-BERT & BERT  & 120 tis. & Wiki, 4 jazyky & 177 M \\ 
	\end{tabular}
	\caption{Meno, použitá architektúra, veľkosť slovníka, dáta použité pri predtrénovaní, počet parametrov modelu (milióny)}
	\label{tab:models}
\end{table}

\subsection{CZERT}

Model B je základný BERT popísaný v \cite{Devlin2018}, a model A je základný ALBERT -- podobný základnému BERT modelu, ale s nižším množstvom parametrov. \cite{Lan2019} \cite{Sido2021}

\subsection{FERNET}

Tento model má rovnakú architektúru ako BERT \cite{Devlin2018}, avšak použitý je iný tokenizér (SentencePiece), ktorý bol expertne vyladený tak, aby boli zachované všetky české znaky a len rozumné množstvo grafém iných jazykov. Je to z toho dôvodu, že v korpuse sa nachádzajú aj texty iných jazykov. \cite{Lehecka2021}

\subsection{RobeCzech}

Tento predtrénovaný model je založený na architektúre RoBERTa \cite{Liu2019a}, ktorý je rozšírením BERTa so zmenami v predtrénovaní. Model sa trénoval dlhšie, väčšími dávkami nad objemnejšími dátami. Takisto bola vyhodená úloha predikcie ďalšej vety (\gls{nsp}), tréning prebehol nad dlhšími sekvenciami a dynamicky sa upravovalo maskovanie slov pri úlohe \gls{mlm}. \cite{Straka2021}

\subsection{SlavicBERT}

Predtrénovaný model je založený na architektúre BERTa \cite{Devlin2018}, avšak s pridanou vrstvou Conditional Random Fields (CRF) \cite{Wallach2004}. Model je viacjazyčný, dokáže spracovať 4 rôzne jazyky (ruština, buhlarčina, čeština a poľština). Radí sa teda medzi viacjazyčné modely. \cite{Arkhipov2019}








\end{document}