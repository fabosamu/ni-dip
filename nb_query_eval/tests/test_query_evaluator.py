import os

import numpy as np
import pytest

cur_dir = os.path.dirname(__file__)


@pytest.mark.parametrize(['keyword', 'candidate_exp'], [
    ('SIPO', ('SIPO', 0)),
    ('pos', ('posta', 2)),
    ('pLaTbA', ('platba', 0)),
])
def test_candidates_and_distances(query_evaluator, keyword, candidate_exp):
    candidates_act = query_evaluator.get_best_candidates_and_distances(keyword)
    assert candidate_exp in candidates_act


@pytest.mark.parametrize(['query', 'mat_exp'], [
    ('sipo platba', [[2., np.nan], [1, np.nan], [2, 2], [np.nan, 1]]),
    ('nic nezhodne', [[1., np.nan], [1, np.nan], [1, 1], [np.nan, 1]]),
    ('sipo platba posta potvrzeni',
     [[2., np.nan], [2, np.nan], [2, 2], [np.nan, 2]]),
])
def test_eval_nb(query_evaluator, tokenizer, kw_matrix_test, query, mat_exp):
    tokens = tokenizer.fit(query)
    query_evaluator.fit(tokens)
    mat_act = query_evaluator.evaluate_query_text_nb(tokens, kw_matrix_test)
    np.testing.assert_array_equal(mat_act.values, mat_exp)


@pytest.mark.parametrize(['query', 'action_exp', 'probas_exp'], [
    ('SiPo', 0, [0.6, 0.4]),
    ('post', 0, [0.6, 0.4]),
    ('PoTvrzeni', 1, [0.4, 0.6]),
    ('PoTvrzeni o platbe', 1, [0.4285714285714286, 0.5714285714285715]),
])
def test_predict(query_evaluator, tokenizer, kw_matrix_test,
                 query, action_exp, probas_exp):
    tokens = tokenizer.fit(query)
    query_evaluator.fit(tokens)
    probas_act = query_evaluator.predict()
    assert probas_act[action_exp] > probas_act[~action_exp]
    np.testing.assert_array_almost_equal(probas_act, probas_exp)


def test_explain(query_evaluator, tokenizer, kw_matrix_test):
    query = 'potvrzeni'
    tokens = tokenizer.fit(query)
    res = query_evaluator.explain(tokens)
    print(res)
