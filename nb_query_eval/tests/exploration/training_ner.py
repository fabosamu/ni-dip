import json
from pathlib import Path

import nb_query_eval.helpers as helpers
from nb_query_eval import Tokenizer, EntityRecognizer
from training_intent import queries_extended, train_test_sticked

cur_dir = Path.cwd()
root_dir = cur_dir / '..' / '..'

if __name__ == "__main__":
    stop_words = helpers.read_stop_words(
        root_dir / 'resources' / 'stopwords.txt')
    currencies = helpers.read_currencies(
        root_dir / 'resources' / 'toy_currencies.csv')
    keyword_matrix = helpers.read_keyword_matrix(
        cur_dir / 'kw_matrix1-21-02-03.csv'
    )
    entity_matrix = helpers.read_entity_matrix(
        cur_dir / 'ent_matrix.csv')
    tokenizer = Tokenizer(stop_words=stop_words)
    queries = str(cur_dir / 'data' / 'query_intents-2021-02-03.csv')

    pe, gb = 5250, 40
    e_evaluator = EntityRecognizer(entity_matrix, currencies,
                                   prior_denominator=pe, general_bf=gb)

    train, test = queries_extended(filename=queries)
    train_x, train_y = train_test_sticked(train, test)

    result = []
    for q, intent in zip(train_x, train_y):
        tks = tokenizer.fit(q)
        tks = e_evaluator.fit(tks)
        _ = e_evaluator.predict()
        result.append({'query': q,
                       'intent': intent,
                       'entities': e_evaluator.entities,
                       'tokens': tks, })

    with open(str(cur_dir / 'query_entities-2021-02-03.json'), 'w',
              encoding='utf8') as f:
        json.dump(result, f, ensure_ascii=False)
