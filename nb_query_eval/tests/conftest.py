import os

import pytest

import nb_query_eval.helpers as helpers
from nb_query_eval import QueryEvaluator, Tokenizer
from nb_query_eval.entity_recognizer import EntityRecognizer

cur_dir = os.path.dirname(__file__)
work_dir = os.path.join(cur_dir, '..', '..')


@pytest.fixture
def kw_matrix_test():
    return helpers.read_entity_matrix(
        os.path.join(cur_dir, 'test_resources', 'test_kw_matrix.csv'))


@pytest.fixture
def ent_matrix_test():
    return helpers.read_entity_matrix(
        os.path.join(cur_dir, 'test_resources', 'test_ent_matrix.csv'))


@pytest.fixture
def currencies():
    return helpers.read_currencies(
        os.path.join(cur_dir, '..', 'resources', 'toy_currencies.csv'))


@pytest.fixture
def stop_words():
    return helpers.read_stop_words(
        os.path.join(cur_dir, '..', 'resources', 'stopwords.txt'))


@pytest.fixture
def tokenizer(stop_words):
    return Tokenizer(stop_words)


@pytest.fixture
def entity_recognizer(ent_matrix_test, currencies):
    return EntityRecognizer(ent_matrix_test, currencies)


@pytest.fixture
def query_evaluator(kw_matrix_test):
    return QueryEvaluator(kw_matrix_test,
                          prior_denominator='auto', general_bf=2,
                          n_best_candidates=1, largest_mistake=2)
