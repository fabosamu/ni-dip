# Podobnosti viet datasetu Banking77

v tomto reporte sú výsledky skúmania podobností vetných embeddingov pomocou rôznych prístupov a v rôznych smeroch (smerom myslím typy viet)

## Prerekvizity

### Kosínová podobnosť 

je definovana ako normalizovaný skalárny súčin: 

$$K(X, Y) = \frac{<X, Y>}{(||X||*||Y||)}$$

kde X a Y sú rozdielne zoznamy vzoriek. Táto definícia je použitá zo [scikitu](https://scikit-learn.org/stable/modules/generated/sklearn.metrics.pairwise.cosine_similarity.html). Funkcia $K(.,.)$ zo `sklearn.metrics.pairwise.cosine_similarity` má na vstupe dva zoznamy vektorov.

Predefinujme pre jednoduchosť túto funkciu po zložkách:

$$K(x, y) = \frac{\langle x, y \rangle}{||x||\cdot||y||}$$

kde $x$ a $y$ sú nejaké vektory reálnych čísel rovnakej veľkosti, $\langle x, y \rangle$ je ich skalárny súčin a $||x||$ je veľkosť vektoru $x$.

Často budeme mať k dispozícií zoznam viet X, kde $x_i$ je prvkom zoznamu (jedna veta).

Ak by sme chceli vypočítať pre zoznam X maticu podobností A (každá veta s každou), použijeme definíciu po zložkách pomocou vyššie definovanej kosínovej podobnosti $K(.,.)$:

$$ A_{i,j} = K(f(x_i), f(x_j)) $$ 

- kde $x_i, x_j \in X$, $i,j \in 1..|X|$
- $f$ je embedovacia funkcia $ f(x) = (a_1, a_2, a_3, ... a_n) $, $a_i \in R$ a $x$ je nejaká veta v prirodzenom jazyku

**! Kedykoľvek v texte použijeme výraz $K("veta1", "veta2")$, myslíme tým výraz $K(f("veta1"), f("veta2"))$ pre zjednodušenie. Typ embedovacej funkcie bude popísaný, ak nie, tak je to LASER !**

### Sledované metriky

- accuracy - acc
- $\texttt{balanced\_accuracy} = \frac{1}{2}\left( \frac{TP}{TP + FN} + \frac{TN}{TN + FP}\right )$
- top_3 accuracy (ak sa daný label vyskytol v prvých troch najpravdepodobnejších miestach klasifikácie)
- f1 skóre
- roc_auc (pre čiastočné výsledky nemožno spočítať)


### použité nástroje:
  - [LASER od facebooku](https://engineering.fb.com/2019/01/22/ai-research/laser-multilingual-sentence-embeddings/) (sentence embeddingy) do 1024 dim priestoru
  - fasttext od facebooku (word embeddingy - pre sentence embeddingy sa L2 normy word_emb spriemerujú do sent. emb.) do 300 dim priestoru
  - [FERNET od ZČU](https://huggingface.co/fav-kky/FERNET-C5) (BERT model ohnutý na sentence embeddingy) do 768 dim priestoru

### Data:

- t2b : dáta od profinitu (text2bank), cca 500 intentov na 13 tried
  - dáta boli ručne upravené a vylepšené o pravopisné, diakritické a iné chyby. Pomocou emimino.cz a googlu som pridal asi tucet intentov ku triedam, kde chýbalo do 20 intentov/trieda
- b77 : dáta z internetov (Banking77), cca 14000 intentov a 77 tried

Poznámka: Vopred sa ospravedlňujem za "volnú mluvu", do diplomky by som si to takto, samozrejme, napísať nedovolil.