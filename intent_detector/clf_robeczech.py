from pathlib import Path
import numpy as np
from transformers import (AutoConfig, AutoModelForSequenceClassification,
                          BertTokenizer, DataCollatorWithPadding, AutoTokenizer, RobertaTokenizer)
from transformers import TrainingArguments, Trainer

from clf import load_merged_banking, preprocess_dataset, compute_metrics, compute_metrics_merged, data_dir

import mlflow
import mlflow.pytorch

mlflow.set_tracking_uri("http://172.16.20.100:5000")

mlflow.set_experiment("text2bank")



def load_robeczech(ROBECZECH_MODEL='ufal/robeczech-base', n_classes=64):
    
    config = AutoConfig.from_pretrained(ROBECZECH_MODEL, num_labels=n_classes)
    tokenizer = RobertaTokenizer.from_pretrained(ROBECZECH_MODEL)

    model = AutoModelForSequenceClassification.from_pretrained(ROBECZECH_MODEL, config=config)
    
    return model, tokenizer



dr_dir = Path('/home/sfabo/ni-dip')



banking_dir = dr_dir / 'data_text2bank/merged-2022-03-24.csv'
dataset = load_merged_banking(banking_dir)

model, tokenizer = load_robeczech(n_classes=np.unique(dataset['train']['labels']).shape[0])

tokenized_dataset = preprocess_dataset(dataset, tokenizer)


data_collator = DataCollatorWithPadding(tokenizer=tokenizer, padding=True)


# let's download fernet
model_name = "ufal/robeczech-base"

training_args = TrainingArguments(
    output_dir=str(data_dir / 'results-transformers' / f'{model_name}-finetuned'),
    learning_rate=2e-6,
    per_device_train_batch_size=8,
    per_device_eval_batch_size=8,
    num_train_epochs=64,
    weight_decay=0.01,

    evaluation_strategy = "epoch",
    save_strategy = "epoch",
    load_best_model_at_end=True,
    metric_for_best_model='acc_bal',
)

trainer = Trainer(
    model=model,
    args=training_args,
    train_dataset=tokenized_dataset["train"],
    eval_dataset=tokenized_dataset["valid"],
    tokenizer=tokenizer,
    data_collator=data_collator,

    compute_metrics=compute_metrics_merged,
)


trainer.train()
