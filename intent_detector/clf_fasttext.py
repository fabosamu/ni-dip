from pathlib import Path
import pickle
from time import time
import json

import click
import fasttext
import pandas as pd
from nb_query_eval import Tokenizer
from nb_query_eval.helpers import read_stop_words
from sklearn.naive_bayes import GaussianNB
from utils import create_md_report, create_report_dir, test_statistics


def words_only(x, tokenizer):
    tks = tokenizer.fit(x)
    return [p[0] for p in tks if p[1] == 'KW']


def preprocess_ft_sent(ctx, fasttext_model_path, col_text_name, col_class_name, use_stopwords):
    ft = fasttext.load_model(fasttext_model_path)
    tkzr = Tokenizer(ctx.obj['stopwords'] if use_stopwords else [])

    df_train = ctx.obj['train']
    df_train['words_list'] = df_train[col_text_name].apply(words_only, tokenizer=tkzr)

    X_train = df_train['words_list'].apply(lambda x: ft.get_sentence_vector(' '.join(x)))
    y_train = df_train[col_class_name]

    df_test = ctx.obj['test']
    df_test['words_list'] = df_test[col_text_name].apply(words_only, tokenizer=tkzr)
    X_test = df_test['words_list'].apply(lambda x: ft.get_sentence_vector(' '.join(x)))
    y_test = df_test[col_class_name]

    return X_train,y_train,X_test,y_test


@click.group()
@click.argument('train_data_path', type=click.File('r'))
@click.argument('test_data_path', type=click.File('r'))
@click.option('--encoding', default='utf-8')
@click.option('--separator', default=',')
@click.option('--report-dir', default=None, type=click.Path(exists=True))
@click.pass_context
def cli(ctx, train_data_path, test_data_path, encoding, separator,
        report_dir):
    ctx.ensure_object(dict)
    ctx.obj['train_path'] = train_data_path
    ctx.obj['test_path'] =  test_data_path
    ctx.obj['train'] = pd.read_csv(train_data_path, sep=separator, encoding=encoding)
    ctx.obj['test'] =  pd.read_csv(test_data_path, sep=separator, encoding=encoding)
    ctx.obj['report_dir'] = Path(report_dir)
    ctx.obj['script_dir'] = Path(globals().get("__file__", "./_")).absolute().parent
    ctx.obj['stopwords'] = read_stop_words(
            ctx.obj['script_dir'] / 'nb_query_eval/resources/stopwords.txt')


@cli.command('ft_nb')
@click.option('--fasttext-model-path', type=click.Path(exists=True))
@click.option('--col-text-name', default='text')
@click.option('--col-class-name', default='category')
@click.option('--test-stats', default=True, is_flag=True)
@click.option('--use-stopwords/--no-use-stopwords', default=True, is_flag=True)
@click.pass_context
def ft_nb(ctx, fasttext_model_path, col_text_name, col_class_name, test_stats, use_stopwords):
    """FastText & Naive Bayes - train, test, report"""

    click.echo('Loading fasttext...')
    ft = fasttext.load_model(fasttext_model_path)
    tkzr = Tokenizer(ctx.obj['stopwords'] if use_stopwords else [])

    df = ctx.obj['train']
    df['words_list'] = df[col_text_name].apply(words_only, tokenizer=tkzr)
    df_prep = df.explode('words_list')

    df_prep['word_emb'] = df_prep['words_list'].apply(ft.get_word_vector)
    X_train = df_prep['word_emb']
    y_train = df_prep[col_class_name]

    df_test = ctx.obj['test']
    df_test['words_list'] = df_test[col_text_name].apply(words_only, tokenizer=tkzr)


    clf = GaussianNB()
    report_path = create_report_dir(ctx.obj['report_dir'], 'ft_nb')
    
    click.echo('Training...')
    start = time()
    clf.fit(X_train.tolist(), y_train.tolist())
    end = time()

    pickle.dump(clf, open(report_path / 'model.pickle', 'wb'))

    click.echo('Predicting test samples...')
    y_pred = []
    for row in df_test.iterrows():
        y_proba_one = clf.predict_proba(
            [ft.get_word_vector(word) for word in row[1]['words_list']])
        y_pred.append(y_proba_one.mean(axis=0).tolist())
    y_test = df_test[col_class_name]

    with open(report_path / 'y_pred.json', 'w') as f:
        json.dump(y_pred, f)


    if test_stats:
        stats = test_statistics(y_test, y_pred, clf.classes_)
        desc = """
        FastText & Gaussian NB on separate word embeddings
        after tokenization using Tokenizer.
        
        X_train_i: Word embedding
        y_pred_i is a mean from word_embdg by word_embdg predictions.
        """
        info = {
            'train time (s)': end - start,
            'train_data_path': ctx.obj['train_path'],
            'test_data_path': ctx.obj['test_path'],
            'fasttext_model_path': fasttext_model_path,
            'column text utterance name': col_text_name,
            'column class (category) name': col_class_name,
            'using stopwords': use_stopwords,
        }
        create_md_report(report_path, desc, info, stats)


@cli.command('ft_nb_sent')
@click.option('--fasttext-model-path', type=click.Path(exists=True))
@click.option('--col-text-name', default='text')
@click.option('--col-class-name', default='category')
@click.option('--test-stats', default=True, is_flag=True)
@click.option('--use-stopwords/--no-use-stopwords', default=True, is_flag=True)
@click.pass_context
def ft_nb_sent(ctx, fasttext_model_path, col_text_name, col_class_name, test_stats, use_stopwords):
    """FastText & Naive Bayes on sentences - train, test, report"""

    click.echo('Loading fasttext, tokenizer and preprocessing...')
    X_train, y_train, X_test, y_test = preprocess_ft_sent(
        ctx, fasttext_model_path, col_text_name, col_class_name, use_stopwords)


    clf = GaussianNB()
    report_path = create_report_dir(ctx.obj['report_dir'], 'ft_nb_sent')
    
    click.echo('Training...')
    start = time()
    clf.fit(X_train.tolist(), y_train.tolist())
    end = time()

    pickle.dump(clf, open(report_path / 'model.pickle', 'wb'))

    click.echo('Predicting test samples...')
    y_pred = clf.predict_proba(X_test.tolist())
   
    with open(report_path / 'y_pred.json', 'w') as f:
        json.dump(y_pred.tolist(), f)


    if test_stats:
        stats = test_statistics(y_test, y_pred, clf.classes_)
        desc = """
        FastText & Gaussian NB on sentence embeddings
        after tokenization using Tokenizer.
        
        X_train_i: Sentence embedding
        y_pred_i is a mean from sent_embdg by sent_embdg predictions.
        """
        info = {
            'train time (s)': end - start,
            'train_data_path': ctx.obj['train_path'],
            'test_data_path': ctx.obj['test_path'],
            'fasttext_model_path': fasttext_model_path,
            'column text utterance name': col_text_name,
            'column class (category) name': col_class_name,
            'using stopwords': use_stopwords,
        }
        create_md_report(report_path, desc, info, stats)


@cli.command('ft_nb_sent_')
@click.option('--fasttext-model-path', type=click.Path(exists=True))
@click.option('--col-text-name', default='text')
@click.option('--col-class-name', default='category')
@click.option('--test-stats', default=True, is_flag=True)
@click.option('--use-stopwords/--no-use-stopwords', default=True, is_flag=True)
@click.pass_context
def ft_nb_sent_(ctx, fasttext_model_path, col_text_name, col_class_name, test_stats, use_stopwords):
    """FastText & Naive Bayes on sentences - selecting best classifier"""

    click.echo('Loading fasttext, tokenizer and preprocessing...')
    X_train, y_train, X_test, y_test = preprocess_ft_sent(
        ctx, fasttext_model_path, col_text_name, col_class_name, use_stopwords)

   




if __name__ == '__main__':
    cli(obj={})
