from pathlib import Path

import numpy as np
import pandas as pd
from scipy.special import softmax
from sklearn.metrics import roc_auc_score, top_k_accuracy_score
from sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import train_test_split

from datasets import ClassLabel, Features, Value, load_dataset, load_metric, DatasetDict, Dataset
from transformers import (AutoConfig, AutoModelForSequenceClassification,
                          BertTokenizer, DataCollatorWithPadding, AutoTokenizer)
from transformers import TrainingArguments, Trainer

from sklearn.metrics import f1_score, top_k_accuracy_score, balanced_accuracy_score, roc_auc_score
from sklearn.preprocessing import label_binarize


data_dir = Path('/data/sfabo')


def load_merged_banking(banking_dir: Path, train_size=0.8, test_rem_size=0.5):
    
    df_merged = pd.read_csv(banking_dir)
    
    cat2label = {k: v for v, k in enumerate(df_merged['kategorie'].unique())}
    df_merged['labels'] = df_merged['kategorie'].map(cat2label)
    
    df_merged_tr, df_merged_tmp = train_test_split(df_merged, train_size=train_size, random_state=42, stratify=df_merged[['kategorie']])
    df_merged_te, df_merged_va = train_test_split(df_merged_tmp, train_size=test_rem_size, random_state=42, stratify=df_merged_tmp[['kategorie']])
    

    banking_features = Features({
        'text_cz': Value('string'), 
        'kategorie': Value('string'), 
        'labels': ClassLabel(names=list(cat2label.keys()))
    })
    
    merged = DatasetDict({
        'train': Dataset.from_pandas(df_merged_tr, features=banking_features),
        'test': Dataset.from_pandas(df_merged_te, features=banking_features),
        'valid': Dataset.from_pandas(df_merged_va, features=banking_features)
    })
    
    return merged


def preprocess_dataset(dataset, tokenizer):

    def preprocess(examples):
        tokens = tokenizer(examples["text_cz"], padding=True, truncation=True)
        return tokens

    tokenized_dataset = dataset.map(preprocess, batched=True)

    return tokenized_dataset


metric_f1 = load_metric("f1")
metric_acc = load_metric("accuracy")


def compute_metrics(eval_pred, labels=list(range(64))):
    logits, labels = eval_pred
    predictions = np.argmax(logits, axis=-1)

    f1 = metric_f1.compute(predictions=predictions, references=labels, average='weighted')['f1']
    acc = metric_acc.compute(predictions=predictions, references=labels)['accuracy']
    
    y_pred = softmax(np.array(logits), axis=1)

    acc_bal = balanced_accuracy_score(y_true=labels, y_pred=np.argmax(y_pred, axis=1))
    acc_top3 = top_k_accuracy_score(y_true=labels, y_score=y_pred, k=3,)

    return {"f1": f1, "acc_bal": acc_bal, "acc": acc, "acc_top_3": acc_top3}


def compute_metrics_partial(eval_pred, start_label=54, end_label=63):
    logits, labels = eval_pred
    predictions = np.argmax(logits, axis=-1)
    # y_pred = softmax(np.array(logits), axis=1)

    l = np.array(labels)
    p = np.array(predictions)

    # end_label = np.max(np.unique(labels))

    part_labels = list(range(start_label, end_label))
    part_idxs = np.isin(l, part_labels)

    l = l[part_idxs]
    p = p[part_idxs]
    # probas = y_pred[part_idxs]

    f1 = metric_f1.compute(predictions=p, references=l, average='weighted')['f1']
    acc = metric_acc.compute(predictions=p, references=l)['accuracy']

    acc_bal = balanced_accuracy_score(y_true=l, y_pred=p)
    # acc_top3 = top_k_accuracy_score(y_true=l, y_score=probas, k=3)

    return {"f1": f1, "acc_bal": acc_bal, "acc": acc}


def compute_metrics_merged_(eval_pred):
    mid, end = 54, 63

    full = compute_metrics(eval_pred)
    b77 = compute_metrics_partial(eval_pred, start_label=0, end_label=mid)
    t2b = compute_metrics_partial(eval_pred, start_label=mid, end_label=end)

    return {'full': full, 'b77': b77, 't2b': t2b}


def compute_metrics_merged(eval_pred):
    mid, end = 54, 63

    full = compute_metrics(eval_pred)
    b77 = compute_metrics_partial(eval_pred, start_label=0, end_label=mid)
    t2b = compute_metrics_partial(eval_pred, start_label=mid, end_label=end)

    return dict(**full,
        **{'b77_' + str(key): val for key, val in b77.items()},
        **{'t2b_' + str(key): val for key, val in t2b.items()},
        )

    # return {'full': full, 'b77': b77, 't2b': t2b}


