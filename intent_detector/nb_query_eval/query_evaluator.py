import numbers
import operator
import unicodedata

import numpy as np
import pandas as pd
from jellyfish import levenshtein_distance
from pandas.core.base import SelectionMixin

from .ensemble import Evaluator


class QueryEvaluator(Evaluator):
    """
    Query classifier based on Naive Bayes.
    Needs a matrix of keywords x actions to compute the final probability of
    action from given query.
    """

    def __init__(self, keyword_matrix: pd.DataFrame, prior_denominator='auto',
                 general_bf=3, n_best_candidates=3,
                 largest_mistake=3) -> None:
        """
        Contructs the QueryEvaluator.

        Parameters
        ----------
        keyword_matrix: pandas.DataFrame
            Predefined matrix for Naive Bayes.

            * Columns: 'keywords' and actions,
            * Rows: 1.0 if keyword matches with action else nan

        prior_denominator: int
            * Prior probability of action classification
                as (1 / prior_denominator).
            * str 'auto' yields prior = 1 / number_of_actions

        general_bf: int
            global multiplier of bayes factors from keyword matrix.
        n_best_candidates: int
            number of suitable candidate keywords
            with smallest levenshtein distance
        largest_mistake: int
            largest levenshtein distance (error) from keyword when predicting.
            Used as correction after obtaining N best candidates
        """
        self.keyword_matrix = keyword_matrix
        if prior_denominator == 'auto':
            self.prior = 1 / len(self.keyword_matrix.columns)
        elif isinstance(prior_denominator, numbers.Number) \
                and prior_denominator > 0:
            self.prior = 1 / prior_denominator
        else:
            raise ValueError('prior_denominator should be number.')
        self.general_bf = general_bf
        self.n_best_candidates = n_best_candidates
        self.largest_mistake = largest_mistake
        self.tokens = []

    def fit(self, tokens):
        """
        Obtain tokens from :class:`Tokenizer` and keep them for prediction

        Parameters
        ----------
        tokens: list
            list of tokens from :class:`Tokenizer`
        action_names: None
            already got from keyword matrix

        Returns
        -------
        list of tokens

        """
        self.tokens = tokens
        return tokens

    def predict(self):
        """
        Naive Bayes classification of fitted tokens.

        1. computes posterior probabilities for each action from prior
        2. normalizes posterior probabilities to 1

        Returns
        -------
        list
            list of probabilities from
            :meth:`QueryEvaluator.evaluate_query_text_nb`
        """
        kw_mat = self.evaluate_query_text_nb(self.tokens,
                                             self.keyword_matrix.copy())
        posterior = self.prior * np.nanprod(kw_mat, axis=0)
        posterior_corr = posterior / (posterior + 1)
        normalized = [x / sum(posterior_corr) for x in posterior_corr]
        action_probas = normalized
        return action_probas

    def predict_proba(self, tokens):
        self.fit(tokens)
        return self.predict()



    def evaluate_query_text_nb(self, tokens, kw_mat):
        """
        Creates posterior keyword matrix

        1. for each keyword token get best candidates
        2. remove candidates with large levenshtein distance
        3. set appropriate row in keyword matrix and correct Bayes Factor (BF)
            multiplied with general BF

        Parameters
        ----------
        tokens: list
            list of tokens from :class:`Tokenizer`
        kw_mat: pandas.DataFrame
            keyword matrix

        Returns
        -------
        pandas.DataFrame
            new computed keyword matrix
        """
        for token in tokens:
            if token[1] == 'KW':
                bests = self.get_best_candidates_and_distances(token[0])
                for best in bests:
                    if best[1] <= self.largest_mistake:
                        rows = kw_mat.loc[best[0]] * self.general_bf
                        kw_mat.loc[best[0]] = rows
        return kw_mat

    def get_best_candidates_and_distances(self, keyword):
        """

        Parameters
        ----------
        keyword: str

        Returns
        -------
        list
            list of [keyword from KW matrix, levenshtein distance from keyword]
            sorted by distance with size of n_best_candidates
        """
        kws = list(self.keyword_matrix.index)
        distances = {kw: levenshtein_distance(keyword.lower(), kw.lower()) for
                     kw in kws}
        best = sorted(distances.items(), key=operator.itemgetter(1))[
               :self.n_best_candidates]
        return best

    def explain(self, tokens):
        self.fit(tokens)
        self.predict()
        kw_mat = self.evaluate_query_text_nb(self.tokens,
                                             self.keyword_matrix.copy())
        res = {}
        for col in kw_mat.columns:
            belongs = list(kw_mat[kw_mat[col] > 1].index)
            if belongs:
                res.update({col: belongs})

        tokens_expl = []
        for token in tokens:
            if token[1] == 'KW':
                bests = self.get_best_candidates_and_distances(token[0])
                for best in bests:
                    if best[1] <= self.largest_mistake:
                        tokens_expl.append((token[0], best[0]))
        return res, tokens_expl


def strip_accents(s):
    return ''.join(c for c in unicodedata.normalize('NFD', s)
                   if unicodedata.category(c) != 'Mn')


class QueryEvaluatorNoAccents(QueryEvaluator):

    def get_best_candidates_and_distances(self, keyword):
        # TODO: edit kw matrix and just apply on words from query
        kws = list(self.keyword_matrix.index)
        distances = {kw: levenshtein_distance(
            strip_accents(keyword.lower()), strip_accents(kw.lower()))
            for kw in kws}
        best = sorted(distances.items(), key=operator.itemgetter(1))[
               :self.n_best_candidates]
        return best
