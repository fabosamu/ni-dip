import pytest
from numpy.testing import assert_array_almost_equal

from nb_query_eval.ensemble import Ensemble


@pytest.fixture
def ensemble(kw_matrix_test, query_evaluator, entity_recognizer, tokenizer):
    return Ensemble(kw_matrix_test.columns,
                    [query_evaluator, entity_recognizer], tokenizer)


@pytest.mark.parametrize(['query', 'proba_idx_exp'], [
    ('SIPO platba', 0),
    ('potvrzeni o platbe', 1),
])
def test_ensemble_fit_predict(ensemble, query, proba_idx_exp):
    ensemble.fit(query)
    probas = ensemble.evaluate()
    print(probas[proba_idx_exp][0], probas[~proba_idx_exp][0])
    assert probas[proba_idx_exp][0] > probas[~proba_idx_exp][0]


def test_weights_computed_correctly(ensemble):
    q = 'posta'
    probas_exp = [.55, .45]
    ensemble.fit(q)

    probas_act = ensemble._predict(weights=None)
    assert probas_act == probas_exp

    probas_act = ensemble._predict(weights=[1, 1])
    assert probas_act == probas_exp

    probas_act = ensemble._predict(weights=[100, 100])
    assert probas_act == probas_exp

    probas_act = ensemble._predict(weights=[.1, .1])
    assert probas_act == probas_exp

    probas_act = ensemble._predict(weights=[1, 0])
    assert_array_almost_equal(probas_act, [.6, .4])

    probas_act = ensemble._predict(weights=[0, 1])
    assert_array_almost_equal(probas_act, [.5, .5])
