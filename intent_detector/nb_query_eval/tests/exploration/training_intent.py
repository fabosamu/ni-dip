from itertools import product
from pathlib import Path

import numpy as np
import pandas as pd
from sklearn.metrics import log_loss
from sklearn.metrics import roc_curve, auc, roc_auc_score
from sklearn.preprocessing import label_binarize

import nb_query_eval.helpers as helpers
from nb_query_eval import QueryEvaluator, Tokenizer, EntityRecognizer, Ensemble
from nb_query_eval.query_evaluator import QueryEvaluatorNoAccents
from text2bank_app.text2bank import create_ensemble

cur_dir = Path.cwd()
root_dir = cur_dir / '..' / '..'


def queries_extended(filename):
    df = pd.read_csv(filename)
    train = df[['dotaz', 'zvolena_akce']].dropna()
    test = df[['dotaz', 'vybrana_akce']].dropna()
    return train, test


def top_1_pred(model, data_x, data_y):
    pred_y3 = []
    for q in data_x:
        model.fit(q)
        pred_y3.append(np.array(model.evaluate(sort_output=True))[:3, 2])

    pred_y = np.array(pred_y3)[:, 0]
    acc = np.mean(data_y == pred_y)
    return pred_y, acc


def extended_list_accuracy(queries_extended, model):
    train, test = queries_extended
    t_df = test.rename(columns={'vybrana_akce': 'zvolena_akce'})
    data = pd.concat([train, t_df], axis=0)

    train_x, train_y = np.array(data)[:, 0], np.array(data)[:, 1]

    pred_y, acc = top_1_pred(model, train_x, train_y)

    df = pd.DataFrame(data={'action': train_y, 'match': train_y == pred_y})

    df['match'] = df['match'].astype(int)

    match = df.groupby('action').mean()
    count = df.groupby('action').count()

    weighted_mean = match['match'] / (count['match'] * 2)
    w_mean = weighted_mean.sum()

    print(match)
    print(count)
    print(match / count)

    assert w_mean == acc


def try_log_loss(model, train_x, train_y):
    result_eval = fit_predict_alphabetically(model, train_x)

    loss = log_loss(train_y, result_eval, labels=sorted(model.action_names))
    assert 0.2 > loss


def train_test_sticked(train, test):
    t_df = test.rename(columns={'vybrana_akce': 'zvolena_akce'})
    data = pd.concat([train, t_df], axis=0)
    train_x, train_y = np.array(data)[:, 0], np.array(data)[:, 1]
    return train_x, train_y


def fit_predict_alphabetically(model, train_x):
    result_eval = []
    for q in train_x:
        model.fit(q)
        eval = model.evaluate()
        eval_sorted = sorted(eval, key=lambda t: t[2], reverse=False)
        result_eval.append(np.array(eval_sorted)[:, 0])
    return result_eval


def grid_search(train_x, train_y, recipe):
    pe = [5250]
    gb = [40]
    nc = [1]
    lm = [2]

    results = []
    for p, g, n, l in product(pe, gb, nc, lm):
        nb_params = dict(
            prior_denominator=p,
            general_bf=g,
            n_best_candidates=n,
            largest_mistake=l,
        )
        recipe['nb_parameters'] = nb_params
        model = create_ensemble(recipe)
        result_eval = fit_predict_alphabetically(model, train_x)
        loss = log_loss(train_y, result_eval, labels=sorted(model.action_names))
        results.append((loss, nb_params))

    results_sorted = sorted(results, key=lambda r: r[0])
    print(results_sorted[:3])


def show_loss_roc(labels, result_eval, train_y):
    loss = log_loss(train_y, result_eval, labels=labels)
    roc = roc_auc_score(train_y, result_eval, average='macro',
                        multi_class='ovo', labels=labels)
    print('loss', loss)
    print('roc', roc)
    train_y_bin = label_binarize(train_y, classes=labels)
    import matplotlib.pyplot as plt
    fpr = dict()
    tpr = dict()
    roc_auc = dict()
    for i in range(len(labels)):
        fpr[i], tpr[i], _ = roc_curve(train_y_bin[:, i],
                                      np.array(result_eval)[:, i])
        roc_auc[i] = auc(fpr[i], tpr[i])
        plt.plot(fpr[i], tpr[i], lw=2,
                 label=f'ROC curve of class {labels[i]} '
                       f'(area = {roc_auc[i]:.2f})')
    plt.legend()
    plt.show()


def train_evaluator(evaluator, tokenizer, train_x, train_y, labels):
    result_eval = []
    for q in train_x:
        tks = tokenizer.fit(q)
        evaluator.fit(tks, None)
        eval = evaluator.predict()
        result_eval.append(eval)

    show_loss_roc(labels, result_eval, train_y)


def train_ensemble(labels, ensemble, train_x, train_y, weights):
    result_eval = []
    for q in train_x:
        ensemble.fit(q)
        eval = np.array(ensemble.evaluate(weights=weights))[:, 0]
        eval = np.array(eval).astype(float)
        result_eval.append(eval)

    show_loss_roc(labels, result_eval, train_y)


def train_evaluator_single_class(evaluator, tokenizer, train_x, train_y,
                                 class_name=None):
    result_eval = []
    print(evaluator.keyword_matrix.columns)
    for q in train_x:
        tks = tokenizer.fit(q)
        evaluator.fit(tks, None)
        eval = evaluator.predict()
        result_eval.append([evaluator.tokens, eval])
    result_eval = np.array(result_eval)

    idxs = np.array(train_y) == 'Potřebuji potvrzení o platbě'
    print(result_eval[idxs])


def generate_kw_mat(path, e_evaluator):
    train, test = queries_extended(filename=path)
    data_x, data_y = train_test_sticked(train, test)
    token_df = []
    for q, c in zip(data_x, data_y):
        tks = tokenizer.fit(q)
        tks = e_evaluator.fit(tks, keyword_matrix.columns)
        for t in tks:
            token_df.append({'token': t[0], 'token_type': t[1], c: 1.0})
    token_df = pd.DataFrame(token_df)
    token_df['token'] = token_df['token'].str.lower()
    token_df = token_df.groupby(['token', 'token_type']).sum()
    cols = token_df.columns
    token_df = token_df.reset_index()
    token_df['sum'] = 0
    for col in cols:
        token_df['sum'] = token_df['sum'] + token_df[col]
        token_df[col] = token_df[col].replace(0, np.nan)
    token_df = token_df.sort_values(['sum', 'token_type', 'token'])
    token_df = token_df[
        (token_df['token_type'] == 'KW') | (token_df['token_type'] == 'SW')]
    token_df = token_df.sort_values(list(cols))
    token_df = token_df[token_df['sum'] > 1]
    print(token_df)
    return token_df


if __name__ == "__main__":
    stop_words = helpers.read_stop_words(
        root_dir / 'resources' / 'stopwords.txt')
    currencies = helpers.read_currencies(
        root_dir / 'resources' / 'toy_currencies.csv')
    keyword_matrix = helpers.read_keyword_matrix(
        # cur_dir / 'kw_matrix.csv'
        # cur_dir / 'kw_matrix-21-02-03.csv'
        cur_dir / 'kw_matrix1-21-02-03.csv'
    )

    entity_matrix = helpers.read_entity_matrix(
        cur_dir / 'ent_matrix04.csv')
    tokenizer = Tokenizer(stop_words=stop_words)
    # queries = os.path.join('data', 'new1.csv')
    queries = str(cur_dir / 'data' / 'query_intents-2021-02-03.csv')

    train, test = queries_extended(filename=queries)
    train_x, train_y = train_test_sticked(train, test)

    pe, gb, nc, lm = 5250, 40, 1, 2

    q_evaluator = QueryEvaluator(keyword_matrix,
                                 prior_denominator=pe, general_bf=gb,
                                 n_best_candidates=nc, largest_mistake=lm)
    e_evaluator = EntityRecognizer(entity_matrix, currencies,
                                   prior_denominator=pe, general_bf=gb)

    qa_evaluator = QueryEvaluatorNoAccents(keyword_matrix,
                                           prior_denominator=pe, general_bf=gb,
                                           n_best_candidates=nc,
                                           largest_mistake=lm)

    ensemble = Ensemble(list(keyword_matrix.columns),
                        [q_evaluator, qa_evaluator, e_evaluator], tokenizer)
    train_ensemble(list(keyword_matrix.columns),
                   ensemble, train_x, train_y, [1, .8, 0])

    # cls = IntentClassifier(prior_denominator=pe, general_bf=gb,
    #                        weights=[1, .8, 0])
    # cv_results = cross_validate(cls, train_x, train_y, cv=3)
    # print(cv_results['test_score'])
    # print(cv_results['score_time'])
