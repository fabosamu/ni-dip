from pathlib import Path

from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.metrics import log_loss

from nb_query_eval import Ensemble, QueryEvaluator, EntityRecognizer, helpers, \
    Tokenizer
from nb_query_eval.query_evaluator import QueryEvaluatorNoAccents


class IntentClassifier(BaseEstimator, ClassifierMixin):
    """An example of classifier"""

    def __init__(self, prior_denominator='auto',
                 general_bf=40,
                 n_best_candidates=1,
                 largest_mistake=2,
                 weights=(1, 1, 0),
                 ):
        """
        Called when initializing the classifier
        """
        self.prior_denominator = prior_denominator
        self.general_bf = general_bf
        self.n_best_candidates = n_best_candidates
        self.largest_mistake = largest_mistake
        self.weights = weights

        cur_dir = Path.cwd()
        root_dir = cur_dir / '..' / '..'

        stop_words = helpers.read_stop_words(
            root_dir / 'resources' / 'stopwords.txt')
        currencies = helpers.read_currencies(
            root_dir / 'resources' / 'toy_currencies.csv')
        keyword_matrix = helpers.read_keyword_matrix(
            cur_dir / 'kw_matrix1-21-02-03.csv'
        )

        entity_matrix = helpers.read_entity_matrix(
            cur_dir / 'ent_matrix.csv')
        tokenizer = Tokenizer(stop_words=stop_words)

        self._labels = list(keyword_matrix.columns)

        q_evaluator = QueryEvaluator(keyword_matrix,
                                     prior_denominator=self.prior_denominator,
                                     general_bf=self.general_bf,
                                     n_best_candidates=self.n_best_candidates,
                                     largest_mistake=self.largest_mistake)
        e_evaluator = EntityRecognizer(entity_matrix, currencies,
                                       prior_denominator=self.prior_denominator,
                                       general_bf=self.general_bf)

        qa_evaluator = QueryEvaluatorNoAccents(
            keyword_matrix,
            prior_denominator=self.prior_denominator,
            general_bf=self.general_bf,
            n_best_candidates=self.n_best_candidates,
            largest_mistake=self.largest_mistake
        )

        self._intent_ensemble = Ensemble(self._labels,
                                         [q_evaluator, qa_evaluator,
                                          e_evaluator],
                                         tokenizer)

    def fit(self, X, y=None):
        """
        This should fit classifier. All the "work" should be done here.

        Note: assert is not a good choice here and you should rather
        use try/except blog with exceptions. This is just for short syntax.
        """

        return self

    def _meaning(self, x):
        self._intent_ensemble.fit(x)
        y = self._intent_ensemble._predict(self.weights)

        return y

    def predict(self, X, y=None):
        return [self._meaning(x) for x in X]

    def score(self, X, y, sample_weight=None):
        result_eval = self.predict(X)
        return - log_loss(y, result_eval, labels=self._labels)
