import os

import numpy as np
import pandas as pd
import pytest

import nb_query_eval.helpers as helpers
from text2bank_app.text2bank import create_ensemble

cur_dir = os.path.dirname(__file__)
os.chdir(os.path.join(cur_dir, '..', '..'))


@pytest.fixture
def keyword_matrix():
    return helpers.read_keyword_matrix(
        os.path.join(cur_dir, '..', 'resources', 'kw_mat_clean0.3.csv'))


@pytest.fixture
def entity_matrix():
    return helpers.read_entity_matrix(
        os.path.join(cur_dir, '..', 'resources', 'entity_matrix0.3.csv'))


@pytest.fixture
def queries():
    df = pd.read_csv(
        os.path.join(cur_dir, 'test_resources', 'query_list.csv')).dropna()
    return df.values


@pytest.fixture
def recipe():
    from text2bank_app.text2bank import read_yaml
    recipe, _ = read_yaml(
        os.path.join(cur_dir, '..', '..', 'recipes', 'dev04.yaml'))
    print(recipe)
    return recipe


@pytest.fixture
def model(recipe):
    from text2bank_app.text2bank import create_ensemble
    return create_ensemble(recipe)


def test_pipeline(recipe):
    ens = create_ensemble(recipe)
    w = 'posli 10e na DE 8937 0400 4405 3201 3000 opakovane'
    tks = ens.fit(w)
    print(tks)
    res = ens.evaluate()
    print(sorted(res, reverse=True))
    print(ens.entities)
    assert 'Trvalé platby' in [r[2] for r in sorted(res, reverse=True)[:3]]


def test_accuracy(model, queries):
    containing = []
    for query in queries:
        q = query[0]
        model.fit(q)
        eval_actions = model.evaluate(sort_output=True)
        probas = [t[2] for t in eval_actions[:3]]
        outcomes = [1 / (i + 1) for i, x in enumerate(probas) if x == query[1]]
        outcome = outcomes[0] if outcomes else 0.
        if not outcomes:
            print(f'query: {q}, eval: {eval_actions[:]}')
        containing.append(outcome)

    mu = sum(containing) / len(containing)
    print('accuracy (based on position in output): ', mu)
    assert mu >= 0.8


def get_top_n_accuracy_report(y_probas, y_true, classes=None, n=3):
    y_probas = np.array(y_probas)
    y_true = np.array(y_true)
    classes = np.array(classes) if classes is not None else sorted(
        np.unique(y_true))
    topn = np.argsort(y_probas, axis=1)[:, -n:]
    exp_proba = classes[topn]

    r = {}
    for i in range(len(y_true)):
        isin = y_true[i] in exp_proba[i]
        r[y_true[i]] = [isin] + r[y_true[i]] if r.get(
            y_true[i]) is not None else [isin]

    results = {k: np.mean(v) for k, v in r.items()}

    return pd.DataFrame.from_dict(results, orient='index', columns=[f'top_{n}'])


def accuracy_report(y_preds, y_true, classes):
    r_1 = get_top_n_accuracy_report(y_probas=y_preds, y_true=y_true,
                                    classes=classes, n=1)
    r_3 = get_top_n_accuracy_report(y_probas=y_preds, y_true=y_true,
                                    classes=classes, n=3)
    ct = np.unique(y_true, return_counts=True)
    r = r_1.join(r_3)
    return r.join(pd.Series(data=ct[1], index=ct[0], name='true_count'))


def obtain_test_accuracy_report(model, queries):
    y_preds = []
    y_true = []
    for query in queries:
        q = query[0]
        model.fit(q)
        pred = model._predict()
        y_preds.append(pred)
        y_true.append(query[1])
    rep = accuracy_report(y_preds, y_true, classes=model.action_names)
    return rep


def _test_top_n_accuracy(model, queries):
    rep = obtain_test_accuracy_report(model, queries)
    print(rep)
    assert not (rep['top_3'] < 0.99).any()  # top_3 accuracy: 1 on testing data


def test_show_waiting(model, queries, tokenizer):
    for i in range(9):
        q = queries[i][0]
        model.fit(q)
        preds = model.evaluate()
        print((q, sorted(preds, reverse=True)))
    rep = obtain_test_accuracy_report(model, queries[:9])
    print(rep)
