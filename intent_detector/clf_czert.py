from pathlib import Path
import numpy as np
from transformers import (AutoConfig, AutoModelForSequenceClassification,
                          BertTokenizer, DataCollatorWithPadding, AutoTokenizer)
from transformers import TrainingArguments, Trainer

from clf import load_merged_banking, preprocess_dataset, compute_metrics_merged, data_dir

import mlflow
import mlflow.pytorch


dr_dir = Path.cwd() # / 'ni-dip'


def load_czert(czert_type='A', n_classes=64):
    # download at https://huggingface.co/UWB-AIR/Czert-B-base-cased
    if czert_type == 'A':
        CZERT_MODEL_PATH = dr_dir / "CZERT-A-v2-czert-albert-base-uncased/czert-base-uncased"
    else:
        CZERT_MODEL_PATH = dr_dir / "czert-bert-base-cased--2020-10-06"
    config = AutoConfig.from_pretrained(CZERT_MODEL_PATH, num_labels=n_classes)

    do_lower_case = True if czert_type == 'A' else False
    tokenizer = BertTokenizer(CZERT_MODEL_PATH / "vocab.txt", strip_accents=False, do_lower_case=do_lower_case)

    model = AutoModelForSequenceClassification.from_pretrained(CZERT_MODEL_PATH, config=config, from_tf=True)

    return model, tokenizer


mlflow.set_tracking_uri("http://172.16.20.100:5000")

mlflow.set_experiment("text2bank")

trsize = 0.7

banking_dir = '/home/sfabo/ni-dip/data_text2bank/merged-2022-03-24.csv'
dataset = load_merged_banking(banking_dir, train_size=trsize)

model, tokenizer = load_czert(czert_type='B', n_classes=np.unique(dataset['train']['labels']).shape[0])

tokenized_dataset = preprocess_dataset(dataset, tokenizer)


data_collator = DataCollatorWithPadding(tokenizer=tokenizer, padding=True)


# let's download fernet
model_name = "CZERT-B-v2"

training_args = TrainingArguments(
    output_dir=str(data_dir / 'results-transformers' / f'{model_name}-finetuned'),
    learning_rate=2e-6,
    per_device_train_batch_size=8,
    per_device_eval_batch_size=8,
    num_train_epochs=32,
    weight_decay=0.01,

    evaluation_strategy = "epoch",
    save_strategy = "epoch",
    load_best_model_at_end=True,
    metric_for_best_model='acc_bal',
)

trainer = Trainer(
    model=model,
    args=training_args,
    train_dataset=tokenized_dataset["train"],
    eval_dataset=tokenized_dataset["valid"],
    tokenizer=tokenizer,
    data_collator=data_collator,

    compute_metrics=compute_metrics_merged,
)


trainer.train()
