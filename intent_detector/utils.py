import json
from pathlib import Path
from time import localtime, strftime

import numpy as np
import sklearn.metrics as mets
from sklearn import preprocessing


def test_statistics(y_test, y_pred, labels):

    lb = preprocessing.LabelBinarizer()
    y_test_b = lb.fit_transform(y_test)

    roc_auc = mets.roc_auc_score(y_test_b, y_pred, multi_class='ovr')
    acc_3 = mets.top_k_accuracy_score(
        y_test, y_pred, k=3, normalize=True, labels=labels)
    acc = mets.top_k_accuracy_score(
        y_test, y_pred, k=1, normalize=True, labels=labels)

    f1 = mets.f1_score(np.argmax(y_test, axis=1), y_pred, average='weighted')

    y_pred_labels = [labels[pred] for pred in np.argmax(y_pred, axis=1)]
    clf_report = mets.classification_report(y_test, y_pred_labels)
    lines = ['\t' + l for l in clf_report.split('\n')]
    clf_report = '\n'.join(lines)

    return dict(
        f1=f1,
        roc_auc=roc_auc,
        accuracy_top_3=acc_3,
        accuracy=acc,
        classification_report=clf_report,
    )


def create_md_report(report_path, description: str, info: dict, statistics: dict):
    ts = strftime("%Y-%m-%d %H:%M:%S", localtime())
    with open(report_path / f'report-{ts}.md', 'w') as m:
        m.write(f'# Report, finished {ts}\n\n')
        m.write(f'{description}\n')
        m.write(f'## Info\n\n')
        for k, v in info.items():
            m.write(f'\t{k}:\t{v}\n')
        m.write('\n')
        m.write(f'## Statistics\n\n')
        m.writelines([f'### {met}\n\n{val}\n\n' for met,
                     val in statistics.items()])
    with open(report_path / f'report-{ts}.json', 'w') as j:
        results = dict(
            description=description,
            **info,
            **statistics,
        )
        json.dump(results, j)


def create_report_dir(report_dir, prefix=''):
    if not report_dir:
        report_dir = Path('./reports')
        report_dir.mkdir(exist_ok=True)

    ts = strftime("%Y%m%d-%H%M%S", localtime())
    report_p = report_dir / f'{prefix}-{ts}'
    report_p.mkdir(exist_ok=False)

    if not report_p.exists():
        raise RuntimeError(f'{report_p} was unable to create!')
    return report_p



