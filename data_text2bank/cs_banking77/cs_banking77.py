# coding=utf-8
# Copyright 2020 The HuggingFace Datasets Authors and the current dataset script contributor.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""BANKING77 dataset."""


import csv
from pathlib import Path

import datasets
from datasets.tasks import TextClassification


_CITATION = """\
@inproceedings{Casanueva2020,
    author      = {I{\~{n}}igo Casanueva and Tadas Temcinas and Daniela Gerz and Matthew Henderson and Ivan Vulic},
    title       = {Efficient Intent Detection with Dual Sentence Encoders},
    year        = {2020},
    month       = {mar},
    note        = {Data available at https://github.com/PolyAI-LDN/task-specific-datasets},
    url         = {https://arxiv.org/abs/2003.04807},
    booktitle   = {Proceedings of the 2nd Workshop on NLP for ConvAI - ACL 2020}
}
"""  # noqa: W605

_DESCRIPTION = """\
Czech BANKING77 dataset provides a very fine-grained set of intents in a banking domain.
This dataset was translated to czech language using Google translation
It comprises 13,083 customer service queries labeled with 77 intents.
It focuses on fine-grained single-domain intent detection.
"""

_HOMEPAGE = "https://github.com/PolyAI-LDN/task-specific-datasets"

_LICENSE = "Creative Commons Attribution 4.0 International"

_TRAIN_DOWNLOAD_URL = (
    "https://raw.githubusercontent.com/PolyAI-LDN/task-specific-datasets/master/banking_data/train.csv"
)
_TEST_DOWNLOAD_URL = "https://raw.githubusercontent.com/PolyAI-LDN/task-specific-datasets/master/banking_data/test.csv"


_cur_dir = Path(globals().get("__file__", "./_")).absolute().parent


class Banking77(datasets.GeneratorBasedBuilder):
    """BANKING77 dataset."""

    VERSION = datasets.Version("1.1.0")

    def _info(self):
        features = datasets.Features(
            {
                "text": datasets.Value("string"),
                "label": datasets.features.ClassLabel(
                    names=[
                        'aktivovat moji kartu',
                        'apple pay nebo google pay',
                        'automatické doplňování',
                        'bezkontaktní nefunguje',
                        'dobití se nezdařilo',
                        'dobití vráceno',
                        'dobíjení kartou',
                        'dobít nabitím kartou',
                        'dobít poplatkem za bankovní převod',
                        'dobít v hotovosti nebo šekem',
                        'doplňte limity',
                        'karta nefunguje',
                        'karta spolkla',
                        'kolík blokován',
                        'kompromitovaná karta',
                        'limity jednorázových karet',
                        'načasování přenosu',
                        'nelze ověřit identitu',
                        'nesprávná částka přijaté hotovosti',
                        'neúspěšný převod',
                        'objednat fyzickou kartu',
                        'odhad doručení karty',
                        'odmítnuta platba kartou',
                        'odmítnutý převod',
                        'odmítnutý výběr hotovosti',
                        'ověřit doplnění',
                        'ověřit mou identitu',
                        'ověřit zdroj financí',
                        'platba inkasem nebyla uznána',
                        'platba kartou nebyla rozpoznána',
                        'platba kartou špatný směnný kurz',
                        'platnost karty brzy skončí',
                        'podpora bankomatu',
                        'podpora fiat měny',
                        'podpora země',
                        'podporované karty a měny',
                        'poplatek za platbu kartou',
                        'poplatek za směnu',
                        'poplatek za výběr hotovosti',
                        'požadovat vrácení peněz',
                        'propojení karet',
                        'proč ověřovat identitu',
                        'převod neobdržel příjemce',
                        'převést na účet',
                        'přijímání karet',
                        'přijímání peněz',
                        'příchod karty',
                        'příjemce není povolen',
                        'příplatek za výpis',
                        'směnný kurz',
                        'transakce účtována dvakrát',
                        'ukončit účet',
                        'upravit osobní údaje',
                        'virtuální karta nefunguje',
                        'vrácení peněz se nezobrazuje',
                        'vrátit platbu kartou?',
                        'vyměnit pin',
                        'vízum nebo mastercard',
                        'výběr hotovosti nebyl uznán',
                        'výměna přes aplikaci',
                        'věkový limit',
                        'zapomenutý přístupový kód',
                        'zrušit převod',
                        'ztracenou nebo odcizenou kartu',
                        'ztracený nebo odcizený telefon',
                        'získat fyzickou kartu',
                        'získat jednorázovou virtuální kartu',
                        'získání náhradní karty',
                        'získání virtuální karty',
                        'zůstatek není aktualizován po bankovním převodu',
                        'zůstatek není aktualizován po šeku nebo vkladu hotovosti',
                        'účtovaný poplatek za převod',
                        'čekající platba kartou',
                        'čekající převod',
                        'čekající výběr hotovosti',
                        'čeká na dobití',
                        'špatný směnný kurz pro výběr hotovosti'
                    ]
                ),
            }
        )
        return datasets.DatasetInfo(
            description=_DESCRIPTION,
            features=features,
            supervised_keys=None,
            homepage=_HOMEPAGE,
            license=_LICENSE,
            citation=_CITATION,
            task_templates=[TextClassification(
                text_column="text", label_column="label")],
        )

    def _split_generators(self, dl_manager):
        """Returns SplitGenerators."""
        train_path = _cur_dir / 'banking-train-translated.csv'
        test_path = _cur_dir / 'banking-test-translated.csv'
        val_path = _cur_dir / 'banking-val-translated.csv'
        return [
            datasets.SplitGenerator(name=datasets.Split.TRAIN, gen_kwargs={
                                    "filepath": train_path}),
            datasets.SplitGenerator(name=datasets.Split.TEST, gen_kwargs={
                                    "filepath": test_path}),
            datasets.SplitGenerator(name=datasets.Split.VALIDATION, gen_kwargs={
                                    "filepath": val_path}),
        ]

        # train_path = dl_manager.download_and_extract(_TRAIN_DOWNLOAD_URL)
        # test_path = dl_manager.download_and_extract(_TEST_DOWNLOAD_URL)
        # return [
        #     datasets.SplitGenerator(name=datasets.Split.TRAIN, gen_kwargs={"filepath": train_path}),
        #     datasets.SplitGenerator(name=datasets.Split.TEST, gen_kwargs={"filepath": test_path}),
        # ]

    def _generate_examples(self, filepath):
        """Yields examples as (key, example) tuples."""
        with open(filepath, encoding="utf-8") as f:
            csv_reader = csv.reader(
                f, quotechar='"', delimiter=",", quoting=csv.QUOTE_ALL, skipinitialspace=True)
            # call next to skip header
            next(csv_reader)
            for id_, row in enumerate(csv_reader):
                text, category, kategorie, text_cz = row
                yield id_, {"text": text_cz, "label": kategorie}
